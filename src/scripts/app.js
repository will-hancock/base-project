﻿// create app namespace
var app = app || {};

// General app function, takes $ - jQuery and App name space
app = (function ($) {

	// Cached elements
	var $views = $('.view'),

		// Init views on page load
		initViews = function () {

			// get views and fire their init's in order of DOM
			$views.each(function () {
				var $view = $(this),
				viewId = $view.attr('id').replace(/-/g, ''),
				viewns = app[viewId] || null;

				if (viewns && viewns.init) {
					viewns.init($view); // pass in DOM element
				}
			});
		};

	return {
		// init function 
		init: function () {

			// Kick it off
			initViews();

		}
	};

})(jQuery);

// jQuery DOM ready, fire app init
$(document).ready(function () {
	app.init();
});