﻿// Home View / Controller

app.home = (function () {

	var $elem,
		
		// Print welcome message to console
		welcomeConsoleMessage = function () {
			console.log('Initialising \'' + $elem.attr('id') + '\' :', $elem);
		};
	


	// Return Init function and any other Public methods
	return {
		init: function ($viewElem) {
			// assign the view elem object to variable within this closure.
			$elem = $viewElem;

			// welcome on init
			welcomeConsoleMessage();
		}
	};
}());